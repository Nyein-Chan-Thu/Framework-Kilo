<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $Id
 * @property string $Company_Name
 * @property string $Address
 * @property string $Phone_Number
 * @property string $Email
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Company_Name', 'Address', 'Phone_Number', 'Email'], 'required'],
            [['Company_Name', 'Address', 'Phone_Number', 'Email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Company_Name' => 'Company Name',
            'Address' => 'Address',
            'Phone_Number' => 'Phone Number',
            'Email' => 'Email',
        ];
    }
}
